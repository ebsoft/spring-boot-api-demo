# Spring Boot API Demo

Create a simple micro-service to re-prioritize list of 10 products. Product has style, color, size 
and priorities. Design and Implement an API that will help me know what products I have, replace a 
product with another one, change product priority, shuffle my products in random (Shuffling will 
change my priorities in random and I have to start over to re-prioritize). Use in-memory data store.
Document assumptions. Add comments to document your thinking or approach.

# Running
`./gradlew clean bootRun`

The application will start running on `localhost:8080` and an initial set of 10 randomly
generated products will be created.

## Endpoints
After the Spring Boot application is running, access it at
`localhost:8080` and test out the endpoints using a tool such as Postman or curl:

* **GET /products** - Retrieve the list of products, ordered by current priority.
* **PUT /product** - Replace or update a product. Including updating its priority. Requires
a JSON request body in the format:
  ```javascript
  {
    "id": 1,
    "style": "my style",
    "color": "my color",
    "size": 10,
    "priority": 95   
  }
  ```
  **Validation:**
  * The ID field must be between 1 and 10, inclusive.
  * Size must be between 1 and 15, inclusive, and priority must be
  between 1 and 100, inclusive.
  * If the product with the given ID is not found, then a 404 Not Found response will be returned.
  * If any of the data fields are missing or incorrect, then a 400 Bad Request response will be returned.


* **POST /products/shuffle** - Shuffle and regenerate the priorities of the products and return the list
  of products, ordered based on the new priorities.