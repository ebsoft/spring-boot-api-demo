package com.yogasoft.api.data;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.springframework.util.Assert;

/**
 * Represents a product with an ID, style, color, size, and priority.
 */
public class Product {

  private final int id;

  private final String style;
  private final String color;

  // TODO: Currently only whole-number sizes allowed. Might need to support partial sizes as well.
  private final int size;

  private final int priority;

  public Product(int id, String style, String color, int size, int priority) {
    // No id validation here, since that generates a 404 elsewhere. (See: ControllerAdvice class)
    Assert.hasText(style, "Style required.");
    Assert.hasText(color, "Color required.");
    Assert.state(size > 0 && size <= 15, "Size greater than 0 and less than or equal to 15 required.");
    Assert.state(priority > 0 && priority <= 100, "Priority greater than 0 and less than or equal to 100 required.");

    this.id = id;
    this.style = style;
    this.color = color;
    this.size = size;
    this.priority = priority;
  }

  public int getId() {
    return id;
  }

  public String getStyle() {
    return style;
  }

  public String getColor() {
    return color;
  }

  public int getSize() {
    return size;
  }

  public int getPriority() {
    return priority;
  }

  @Override
  public boolean equals(Object obj) {
    return EqualsBuilder.reflectionEquals(this, obj);
  }

  @Override
  public int hashCode() {
    return HashCodeBuilder.reflectionHashCode(this);
  }
}
