package com.yogasoft.api.data;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.stereotype.Component;

/**
 * Simple in-memory data store for demo app.
 *
 * Note, this is not optimized for a multi-threaded environment and might be unsafe for high-load
 * situations where multiple requests are modifying data or performing operations in parallel.
 */
@Component
public class DataStore {

  private final Random random = new Random();

  private SortedSet<Product> productsByPriority = createNewSortedSet();

  public DataStore() {
    generateTenProducts();
  }

  public List<Product> getAllProducts() {
    // Unmodifiable lists returned for basic protection of internal Set. Doesn't necessarily protect
    // the actual Product objects from unintentional modification though.
    return List.copyOf(productsByPriority);
  }

  /**
   * Allows replacing a product based on the product's id field. Can be used to change data or
   * change the priority of the product.
   */
  public void replaceOrUpdateProduct(Product product) {
    // Verify the product exists by attempting to remove it first.
    if (!productsByPriority.removeIf(p -> p.getId() == product.getId())) {
      throw new ProductNotFoundException(product.getId());
    }
    productsByPriority.add(product);
  }

  /**
   * Randomly re-generate product priorities.
   *
   * Note, could cause strange or inconsistent behavior if this gets called in parallel on multiple
   * threads and/or while the data is being processed in other method calls. Would need to optimize
   * for multi-threading performance.
   *
   * Returns the newly prioritized products.
   */
  public List<Product> shuffleProductPriorities() {
    return List.copyOf(productsByPriority = productsByPriority.stream()
        .map(product -> new Product(product.getId(),
                                    product.getStyle(),
                                    product.getColor(),
                                    product.getSize(),
                                    generateRandomPriority()))
        .collect(Collectors.toCollection(this::createNewSortedSet)));
  }

  /**
   * Generates ten new products with random id between 1 and 10, inclusive. Style and color strings
   * are randomly generated and also generates random size between 1 and 15 and a random priority
   * from 1 to 100.
   *
   * It is possible for multiple products to have the same priority, but not for them to have the
   * same id.
   */
  private void generateTenProducts() {
    IntStream.range(1, 11).forEach(id -> productsByPriority.add(new Product(
        id,
        "style-" + RandomStringUtils.randomAlphabetic(5),
        "color-" + RandomStringUtils.randomAlphabetic(4),
        random.nextInt(15) + 1,
        generateRandomPriority())));
  }

  /**
   * Generate a random priority between 1 and 100, inclusive.
   */
  private int generateRandomPriority() {
    return random.nextInt(100) + 1;
  }

  /**
   * All products ordered by priority. Synchronized for basic thread safety.
   */
  private SortedSet<Product> createNewSortedSet() {
    return Collections
        .synchronizedSortedSet(new TreeSet<>(
            // Must compare secondarily by id to handle the case of duplicate priorities.
            Comparator.comparing(Product::getPriority).thenComparing(Product::getId)));
  }
}