package com.yogasoft.api.rest;

import com.yogasoft.api.data.DataStore;
import com.yogasoft.api.data.Product;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * Main REST API controller.
 *
 * TODO: Add some Swagger or other documentation tool annotations for generating API documentation.
 */
@RestController
public class RestAPI {

  private final DataStore dataStore;

  @Autowired
  public RestAPI(DataStore dataStore) {
    this.dataStore = dataStore;
  }

  @GetMapping(path = "/products")
  public List<Product> getProducts() {
    return dataStore.getAllProducts();
  }

  // TODO Body validation should be added using javax.validation, but not implemented yet.
  @PutMapping(path = "/product")
  public void replaceOrUpdateProduct(@RequestBody Product product) {
    dataStore.replaceOrUpdateProduct(product);
  }

  /**
   * Shuffles all product priorities, then returns the newly ordered list of products.
   */
  @PostMapping(path = "/products/shuffle")
  public List<Product> shuffleProductPriorities() {
    return dataStore.shuffleProductPriorities();
  }
}
