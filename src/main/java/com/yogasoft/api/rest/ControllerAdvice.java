package com.yogasoft.api.rest;

import com.yogasoft.api.data.ProductNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;

/**
 * Handles known exceptions to return an appropriate response status and error body.
 */
@RestControllerAdvice
public class ControllerAdvice {

  @ExceptionHandler({IllegalStateException.class, IllegalArgumentException.class})
  @ResponseStatus(HttpStatus.BAD_REQUEST)
  public DemoError badRequest(Exception ex, WebRequest request) {
    String msg = ex.getCause() != null ? ex.getCause().getMessage() : ex.getMessage();
    return new DemoError(msg);
  }

  @ExceptionHandler({ProductNotFoundException.class})
  @ResponseStatus(HttpStatus.NOT_FOUND)
  public DemoError notFound(Exception ex, WebRequest request) {
    return new DemoError(ex.getMessage());
  }
}
