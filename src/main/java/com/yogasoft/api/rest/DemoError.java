package com.yogasoft.api.rest;

/**
 * Simple error class representing the format of a handled error response body.
 */
public class DemoError {

  private final String error;

  public DemoError(String error) {
    this.error = error;
  }

  public String getError() {
    return error;
  }
}
